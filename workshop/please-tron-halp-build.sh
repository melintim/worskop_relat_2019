#!/bin/bash

module purge
module load intel/17.0+mkl
module load impi/2017
module load python/2.7.9
module load cmake/3.8.2

export MATH_ROOT=$MKLROOT
export FC=ifort
export CC=icc
export CXX=icpc

./setup --mpi --fc=mpiifort --cc=mpiicc --cxx=mpiicpc

cd build

make -j4

